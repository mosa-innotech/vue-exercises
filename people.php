<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>API Demo</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/vue.css">
    </head>
    <body>
        <h1 id="title">{{ title }}</h1>
        <br /><br />

        <div id="">

            <div class="row">

                <div class="col-md-6">

                    <div id="people">
                        <div v-for="person in people" class="PersonBox">
                            ID: {{ person.id }} <br />
                            Name: {{ person.name }} <br />
                            Age: {{ person.age }} <br />
                            Address: {{ person.city }} <br />
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div id="tweets">

                        <div v-for="tweet in tweets" class="PersonBox">

                            <div class="tweetbox">
                                {{ tweet.tweet }}
                            </div>

                            <div class="tweet-support">
                                <img :src="tweet.user_img" /> {{ tweet.user_name }}, {{ tweet.date }} - Likes {{ tweet.likes }}
                                &nbsp;&nbsp;
                                <button class="small-button">Like</button>
                                <button class="small-button">Delete</button>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </div>

        <?php include ("partials/footer.php");?>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/vue.min.js"></script>
        <script type="text/javascript" src="js/index.js"></script>
    </body>
</html>
