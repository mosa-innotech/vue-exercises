var d = new Date();
var currentYear = d.getFullYear();
var title = "Vue Exercises";

var tweets = [
    { id: "1", user_name: "Mosa A", tweet: "This is my first Tweet" , date: "Mon Dec 23, 2018" , likes: "21", user_img: "https://media1.giphy.com/media/SggILpMXO7Xt6/200w.gif"},
    { id: "2", user_name: "Rachel K", tweet: "This is my second Tweet", date: "Tuesday Dec 24, 2018" , likes: "12", user_img: "https://media1.giphy.com/media/SggILpMXO7Xt6/200w.gif"},
    { id: "3", user_name: "Megha B", tweet: "My phone is awesome", date: "Wednesday Dec 24, 2018" , likes: "67", user_img: "https://media1.giphy.com/media/SggILpMXO7Xt6/200w.gif"},
    { id: "4", user_name: "Brittany H", tweet: "School sucks", date: "Sunday Dec 20, 2018" , likes: "3", user_img: "https://media1.giphy.com/media/SggILpMXO7Xt6/200w.gif"},
    { id: "5", user_name: "Bushra L", tweet: "I've done it already", date: "Friday Dec 28, 2018" , likes: "0", user_img: "https://media1.giphy.com/media/SggILpMXO7Xt6/200w.gif"},
];


Vue.component('single-tweet', {
    props: ['tweet'],
    data: function () {
        return {
            likes: this.tweet.likes
        }
    },
    template: `
        <div>
            <div class="tweetbox">
                {{ tweet.tweet }}
            </div>

            <div class="tweet-support">
                <img :src="tweet.user_img" /> {{ tweet.user_name }}, {{ tweet.date }} - Likes <span v-bind:"tweet.likes" ></span> 
                &nbsp;&nbsp;
                <button class="small-button">Like</button>
                <button class="small-button">Delete</button>
            </div>
        </div>
    `
})


new Vue({
    el: '#tweets',
    data: {
        tweets : tweets
    }
 })
 new Vue({
     el: '#people',
     data: {
         people : {
           "1": { id: "001", name: "Mosa", age: 13 , city: "Edmonton"},
           "2": { id: "456", name: "Rachel", age: 23 , city: "Miami" },
           "3": { id: "789", name: "Megha", age: 25  , city: "Toronto"},
           "4": { id: "101", name: "Bushra", age: 17  , city: "Vancouver"},
           "5": { id: "102", name: "Brittany", age: 29  , city: "Calgary"}
         }
     }
  })


 new Vue({
     el: '#restaurants',
     data: {
         restaurants : ["MacDs", "KFC", "Subway", "Opa", "Earls"]
     }
  })

new Vue({
  el: '#title',
  data: {
    title: title
  }
})

new Vue({
  el: '#footer',
  data: {
    text: '\u00A9 ' + currentYear + " "+ title
  }
})



// Define a new component called button-counter
Vue.component('single-tweet', {
    props: ['likes'],
    data: function () {
        return {
            likes: this.likes
        }
    },
    template: `<div>
        <div class="tweetbox">
            {{ likes }}
        </div>
    </div>`
})
